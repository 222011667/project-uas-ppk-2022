package com.example.uasproject;

public class Ayat {
    private String text, title;
    private int noAyat;

    public Ayat(String text, String title, int noAyat) {
        this.text = text;
        this.title = title;
        this.noAyat = noAyat;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNoAyat() {
        return noAyat;
    }

    public void setNoAyat(int noAyat) {
        this.noAyat = noAyat;
    }
}
