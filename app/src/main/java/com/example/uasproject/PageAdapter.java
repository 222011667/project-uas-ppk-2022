package com.example.uasproject;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class PageAdapter extends RecyclerView.Adapter<TextHolder> {
    ArrayList<Ayat> ayats;

    public PageAdapter(ArrayList<Ayat> ayats) {
        this.ayats = ayats;
    }

    @NonNull
    @Override
    public TextHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.text_holder, parent,false);
        return new TextHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TextHolder holder, int position) {
        holder.getTextAyat().setText("("+this.ayats.get(position).getNoAyat()+")"+this.ayats.get(position).getText());
    }

    @Override
    public int getItemCount() {
        return this.ayats==null?0:this.ayats.size();
    }
}
