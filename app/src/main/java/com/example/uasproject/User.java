package com.example.uasproject;

public class User {
    String username;
    int lastRead;

    public User(String username, int lastRead) {
        this.username = username;
        this.lastRead = lastRead;
    }

    public String getUsername() {
        return username;
    }

    public int getLastRead() {
        return lastRead;
    }

    public void setLastRead(int lastRead) {
        this.lastRead = lastRead;
    }
}
