package com.example.uasproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    Button btnStartRead, btnLastRead, btnSuratList, btnLogin, btnRegister;
    TextView txtUsername, txtLastRead;
    LinearLayout profileView;

    static User activeUser = null;
    static boolean isLoggedIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        isLoggedIn = getIntent().getBooleanExtra("isLoggedIn",false);
        if (isLoggedIn){
            activeUser = new User(
                    getIntent().getStringExtra("username"),
                    getIntent().getIntExtra("lastRead",1)
            );
            this.txtUsername.setText(activeUser.getUsername());
            this.txtLastRead.setText(Integer.toString(activeUser.getLastRead()));

            this.profileView.setVisibility(View.VISIBLE);
            this.btnLastRead.setVisibility(View.VISIBLE);
            this.btnLogin.setVisibility(View.GONE);
            this.btnRegister.setVisibility(View.GONE);
        }else {
            this.profileView.setVisibility(View.GONE);
            this.btnLastRead.setVisibility(View.GONE);
            this.btnLogin.setVisibility(View.VISIBLE);
            this.btnRegister.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.txtLastRead!=null && activeUser!=null){
            this.txtLastRead.setText(Integer.toString(activeUser.getLastRead()));
        }
    }

    public void init(){
        this.btnStartRead = findViewById(R.id.btnStartRead);
        this.btnLastRead = findViewById(R.id.btnLastRead);
//        this.btnSuratList = findViewById(R.id.btnSuratList);
        this.btnLogin = findViewById(R.id.btnLogin);
        this.btnRegister = findViewById(R.id.btnRegister);
        this.profileView = findViewById(R.id.profileView);
        this.txtUsername = findViewById(R.id.txtUsername);
        this.txtLastRead = findViewById(R.id.txtLastRead);

        btnStartRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getThisClassContext(),ReadingPageActivity.class);
                intent.putExtra("currentPage", 1);
                startActivity(intent);
            }
        });
        btnLastRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getThisClassContext(),ReadingPageActivity.class);
                intent.putExtra("currentPage", activeUser.getLastRead());
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getThisClassContext(),LoginActivity.class);
                startActivity(intent);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getThisClassContext(),RegisterActivity.class);
                startActivity(intent);
            }
        });

    }

    private Context getThisClassContext(){
        return this;
    }


}