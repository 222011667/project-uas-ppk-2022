package com.example.uasproject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

public class FetchPage implements Runnable {

    private final AtomicBoolean running = new AtomicBoolean(false);
    private JSONObject data;
    private String urlString;

    public FetchPage(String urlString) {
        this.urlString = urlString;
    }

    public void start() {
        Thread worker = new Thread(this);
        worker.start();
    }

    public void stop() {
        running.set(false);
    }

    @Override
    public void run() {
        running.set(true);
        while (running.get()) {
            //What to do
            URL url = null;
            StringBuilder data = new StringBuilder();
            try {
                url = new URL(urlString);
                HttpURLConnection httpURLConnection;
                if (url != null) {
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = bufferedReader.readLine()) != null){
                        data.append(line);
                    }
                    bufferedReader.close();
                    httpURLConnection.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (data.length() > 0){
                try {
                    this.data = new JSONObject(data.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            stop();
        }
    }

    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }

    public JSONObject getData() {
        try {
            return (JSONObject) data.get("data");
        } catch (Exception r){
            r.printStackTrace();
        }
        return null;
    }
    public boolean isRunning(){
        return this.running.get();
    }
}