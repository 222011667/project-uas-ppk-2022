package com.example.uasproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {
    EditText editUsername, editPassword;
    Button btnLoginProcess;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
    }

    public void init(){
        this.editUsername = findViewById(R.id.editUsername);
        this.editPassword = findViewById(R.id.editPassword);
        this.btnLoginProcess = findViewById(R.id.btnLoginProcess);

        this.btnLoginProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username, password;

                username = editUsername.getText().toString();
                password = editPassword.getText().toString();

                Intent intent = new Intent(getThisClassContext(),MainActivity.class);
                intent.putExtra("isLoggedIn",true);
                intent.putExtra("username",username);
                intent.putExtra("lastRead",17);
                startActivity(intent);
            }
        });
    }

    private Context getThisClassContext(){
        return this;
    }
}