package com.example.uasproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReadingPageActivity extends AppCompatActivity {
    Button btnNext, btnPrev, btnSave;
    EditText editPage;
    ProgressBar progressBar;
    RecyclerView recyclerView;

    int currentPage;
    ArrayList<String> data;
    ArrayList<Ayat> ayats;

    FetchPage fetchPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reading_page);

        init();

        if (MainActivity.isLoggedIn){
//            this.currentPage = MainActivity.activeUser.getLastRead();
            this.btnSave.setVisibility(View.VISIBLE);
        }else {
            this.btnSave.setVisibility(View.GONE);
        }
    }

    private void init(){
        this.btnNext = findViewById(R.id.btnNext);
        this.btnPrev = findViewById(R.id.btnPrev);
        this.btnSave = findViewById(R.id.btnSave);
        this.editPage = findViewById(R.id.page);
        this.progressBar = findViewById(R.id.progressBar);
        this.recyclerView = findViewById(R.id.recyclerView);
        this.data = new ArrayList<>();
        this.ayats = new ArrayList<>();
        this.fetchPage =  new FetchPage("https://api.alquran.cloud/v1/page/2/quran-uthmani");

        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        changePage(getIntent().getIntExtra("currentPage",1));

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextPage();
            }
        });
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prevPage();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savePage();
            }
        });
        this.editPage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                int newPage = Integer.parseInt(editPage.getText().toString());
                if (newPage>604) newPage = 604;
                if (newPage<1) newPage = 1;
                changePage(newPage);
            }
        });
    }

    public void fetchData(){
        this.fetchPage.start();
        progressBar.setVisibility(View.VISIBLE);
        while (fetchPage.isRunning() || fetchPage.getData() == null){
            System.out.println("loading");
        }
        progressBar.setVisibility(View.GONE);
        System.out.println("DONE");

    }
    public void fetchData(String newURL){
        this.fetchPage.setUrlString(newURL);
        fetchData();
    }
    public void changePage(int newPage){
        try {
            fetchData("https://api.alquran.cloud/v1/page/"+newPage+"/quran-uthmani");
            this.currentPage = (int) fetchPage.getData().get("number");
            this.editPage.setText(Integer.toString(this.currentPage));

            JSONArray jsonArray = (JSONArray) fetchPage.getData().get("ayahs");

            this.ayats.clear();
            int i = 0;
            while (i < jsonArray.length()){
                JSONObject data = (JSONObject) jsonArray.get(i);
                String text = (String) data.get("text");
                int noAyat = (int) data.get("numberInSurah");
                data = (JSONObject) data.get("surah");
                String title = (String) data.get("englishName");

                Ayat ayat = new Ayat(text,title,noAyat);

                this.ayats.add(ayat);
                i++;
            }

            this.recyclerView.setAdapter(new PageAdapter(this.ayats));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void nextPage(){
        int newPage = this.currentPage + 1;
        if (newPage>604) newPage = 604;
        changePage(newPage);
    }
    public void prevPage(){
        int newPage = this.currentPage - 1;
        if (newPage<1) newPage = 1;
        changePage(newPage);
    }
    public void savePage(){
        MainActivity.activeUser.setLastRead(currentPage);
        Toast.makeText(this,"LAST READ CHANGED TO "+currentPage,Toast.LENGTH_SHORT).show();
    }
}
