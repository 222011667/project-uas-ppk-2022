package com.example.uasproject;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TextHolder extends RecyclerView.ViewHolder {
    String text;
    TextView textAyat;

    public TextHolder(@NonNull View itemView) {
        super(itemView);
        this.textAyat = itemView.findViewById(R.id.textAyat);
    }

    public TextView getTextAyat() {
        return textAyat;
    }
}
